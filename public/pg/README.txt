ftp://ftp.stratus.com/pub/vos/multics/README.txt
Paul Green (Paul.Green@stratus.com)

Last Updated 2011-06-16.

This directory contains various Multics commands that I wrote or
collected.  Most are from Multics; a few are from Stratus (the
ones with calls to s$xxx subroutines).  It also contains my
paper on the Multics Virtual Memory.

The following programs are available:

     case.pl1                   Multics "case" active function.
     clock_speed.pl1            Multics program to time clock bif.
     cookie.pl1                 Multics "cookie" command. (by Chris Tavares)
     copy_bad_mst.pl1           Multics program to copy Multics tape.
     copy_tape.pl1              Stratus-on-Prime program to copy tape.
     create_moo_ladder_file.pl1 Stratus program to create the "moo"
                                ladder file.
     display_descriptor.pl1     Multics cmd to decode arg descriptor.
     display_tape_status.pl1    Multics program to decode tape status.
     dump_tape.pl1              Stratus-on-Prime program to dump tape.
     gomoku.pl1                 Multics program to play gomoku game.
     gomoku_move.pl1            Multics subrs for gomoku.pl1.
     holidays.ec                Multics exec_com (shell) script to
                                compute holiday dates.
     moo.orig.pl1               Original Multics pgm to play moo game.
     moo.pl1                    Cleaned-up moo program.
     mosaic.info                Info about configuring Mosaic viewer.
     multics.ps                 Postscript for Multics Ring Logo.
     plot_profile.info          Multics help on plot_profile command.
     plot_profile.pl1           Multics cmd to plot compiler profile.
     read_tape.pl1              Stratus-on-Prime command to read tape.
     roman.pl1                  Multics active function to convert
                                arabic to roman numerals.
     salvage_tape.pl1           Multics pgm to salvage Multics tape.
     trek.old.pl1               Original program to play startrek game.
     trek.pl1                   Modified program to play startrek game.

The following text files are available:

     mvm.txt                    Multics Virtual Memory Paper (ASCII)
     mvm.html                   Multics Virtual Memory Paper (HTML)
     prime_numbers              List of first 2050 prime numbers.

(end)
