
MULTICS ADMINISTRATIVE BULLETIN                           MAB-070


To:       MAB Distribution

From:     C. T. Clingen
          J. M. Stansbury

Date:     August 14, 1985

Subject:  Multics Configuration Management:  Policy Statement


ABSTRACT

This  bulletin establishes   management policy  regarding Multics
Configuration  Management.  The Multics  Configuration Management
system controls changes to  design documentation, system software
modules and user documentation,  and functional test software and
documentation.    It  assures    a  consistent   mapping  between
documentation  and code  associated with  the current  version of
Multics System Software and, in particular, the Trusted Computing
Base (TCB).

Strict adherence  to this policy is required  by contributors and
managers within the Multics Development Center (MDC), and also by
any person  or group contracting  to develop or  maintain Multics
system  software   or  documentation.   The  only   exception  is
self-contained software developed by  third parties, as described
in MAB-064.





















_________________________________________________________________

Multics  Project  internal  working  documentation.   Not  to  be
reproduced or distributed outside the Multics Project.


                              - i -

MAB-070                           Configuration Management Policy









                             CONTENTS


                                                         Page

    1:  References  . . . . . . . . . . . . . . . . . .   1
    2:  Introduction  . . . . . . . . . . . . . . . . .   2
    2.1:  Purpose of the Policies . . . . . . . . . . .   2
    2.2:  Scope of the Policies . . . . . . . . . . . .   2
    2.3:  Exceptions to the Policies  . . . . . . . . .   3
    3:  People Involved in Configuration Management . .   3
    3.1:  Software Designer/Developer . . . . . . . . .   3
    3.2:  Design Reviewers  . . . . . . . . . . . . . .   3
    3.3:  Design Approvers  . . . . . . . . . . . . . .   4
    3.4:  Auditor . . . . . . . . . . . . . . . . . . .   4
    3.5:  Security Coordinator  . . . . . . . . . . . .   4
    3.6:  Documentation Personnel . . . . . . . . . . .   4
    3.7:  Software Development Manager  . . . . . . . .   4
    3.8:  System Integration Project  . . . . . . . . .   5
    4:  Policies for Planned Changes  . . . . . . . . .   5
    5:  Policies for Emergency Changes  . . . . . . . .   7
    6:  Policies for Critical Fix Changes . . . . . . .   7

























                              - ii -

Configuration Management Policy                           MAB-070


1:  REFERENCES

Multics  Configuration  Management  Documents,  which  constitute
Multics Configuration Management Policy and procedures.

1.   MAB-066, August  8, 1985, Multics  Configuration Management:
     Software Development Procedures, by B.  I.  Margulies and R.
     Barstad

2.   MAB-048,  August  12,  1985,  Rules  for  the Multics Change
     Review Board, by R.  Barstad

3.   MAB-069, August 13,  1985, Multics Configuration Management:
     Guidelines for Auditing Software, by G.  C.  Dixon

4.   MAB-056, August  7, 1985, Multics  Configuration Management:
     Installing  Planned Changes  in System  Libraries by  F.  W.
     Martinson and G.  E.  Johnson

5.   MAB-057, August  7, 1985, Multics  Configuration Management:
     Installing Emergency Changes in  System Libraries, by G.  E.
     Johnson

6.   MAB-063, August 13,  1985, Multics Configuration Management:
     Critical Fixes for Released Software, by G.  C.  Dixon

7.   MAB-067,   August   5,   1985,   Procedures   for   Software
     Installation and Integration, by W.  Olin Sibert

8.   MAB-068, August 13, 1985,  Multics Programming Standards, by
     B.  Margulies and R.  Barstad

9.   MAB-071,  August  14,  1985,  Multics  Security Coordinator:
     Duties and Responsibilities, by J.  M.  Stansbury

Additional  references which  provide relevant  administrative or
technical information:

10.  MDD-004, July 10, 1985, The Multics Security Functional Test
     Suite:  Goals, Standards and Policies, by E.  A.  Ranzenbach

11.  MAB-064,  August 27,  1984, Third  Party Software Submission
     Procedures, by F.  W.  Martinson

12.  Department  of Defense   Trusted Computer  System Evaluation
     Criteria, CSC-STD-001-83, 15 August 1983, also known as "The
     Orange Book"

13.  MTB-716,   "Multics   Configuration   Management:   Tracking
     Software Changes in  MR12.0", by G.  C.  Dixon  and W.  Olin
     Sibert



                              - 1 -

MAB-070                           Configuration Management Policy


2:  INTRODUCTION

This document defines  Multics Configuration Management policies,
and  indicates subsidiary  documents which  provide details about
these  policies,  as  well   as  procedures  and  guidelines  for
implementing the policies.  Configuration  management is a system
for  controlling  the  way  changes  are  made  to Multics design
documentation,  implementation  documentation,  software modules,
and functional test software and documentation.


2.1:  Purpose of the Policies

The purpose of the Configuration Management system is to define a
set  of policies  and procedures  which ensure  that the  Multics
system  is composed of  high quality software  and documentation.
An important goal of the system is to ensure that Multics retains
a Department of Defense  Computer Security Center (DoDCSC) rating
of at least B2.


2.2:  Scope of the Policies

The  Configuration  Management  system  applies  only to software
destined for installation in the  system libraries and release to
customers.  Other software (e.g.,  private tools) need not follow
them.  Specifically, the policies apply to the following items.

o    Released Product Software (TCB and Non-TCB)
o    Published Product Documentation
o    TCB Design Documentation
o    TCB Security Functional Tests
o    TCB Functional Test Support Software

The term Trusted  Computing Base (TCB) is defined as  part of the
DoDCSC  Evaluation  Criteria  [ref  12].   For  Multics,  the TCB
consists of the following items.

o    The hardware and associated micro-code.
o    All software that executes in ring zero and ring one.
o    All software that executes in the front-end processor.
o    All  user-ring  subsystems  requiring  access  to gates that
     circumvent the normal access  requirements for rings greater
     than  one, such  as system_privilege_  and hphcs_.  Examples
     are the  I/O Daemon, the  backup systems, and  the Answering
     Service.


Three   types  of   software  changes   are  controlled   by  the
Configuration Management system.  These are:

o    Planned changes to prerelease software and documentation.


                              - 2 -

Configuration Management Policy                           MAB-070


o    Emergency changes to prerelease software and documentation.

o    Critical fix changes to released software and documentation.

Each type of  change is controlled by a separate  set of policies
and procedures, as summarized below.


2.3:  Exceptions to the Policies

No  exceptions to  these  policies  and procedures  are permitted
without   the  explicit   approval  of   the  Director,   Multics
Development Center.  The Director will retain complete records of
any  such  exceptions  permitted.    An  exception  is  regularly
permitted for "Self-Contained Software"  developed by third party
software vendors, as defined in MAB-064 [ref 11].


3:  PEOPLE INVOLVED IN CONFIGURATION MANAGEMENT

Before  outlining  the  configuration  management  policies,  the
following   paragraphs   introduce   the   people   involved   in
implementing  these policies.  These  people are all  involved in
making planned changes  to the system, and some of  them are also
involved in emergency and critical fix changes as well.


3.1:  Software Designer/Developer

The  software  designer/developer  is   the  key  person  in  the
configuration management process.   Configuration management is a
system for controlling  software changes.  The designer/developer
is  the person  who conceives  of and  designs a  change, and who
submits functional descriptions of the change for peer review and
approval.   The  developer  implements   and  tests  the  change,
provides   technical  information   to  the   documentation  unit
describing  the change,  and finally  submits the  change to  the
auditor for implementation review and approval.  See MAB-066 [ref
1] for more information.


3.2:  Design Reviewers

Design reviewers  are peers of  the developer who  have the time,
interest and expertise to review software change designs proposed
by  the developer.   Reviews are  held informally  via electronic
meeting or teleconference to help the developer refine the change
proposal,   resolve  problems   in  the   proposal,  etc.    More
information on this is also in MAB-066 [ref 1].





                              - 3 -

MAB-070                           Configuration Management Policy


3.3:  Design Approvers

All  designs are  approved by   the Multics  Change Review  Board
(MCRB), prior to their installation.   For each change, the board
reviews  a formal  Multics Change  Request (MCR)  provided by the
developer.   The purpose  of the   review is  to ensure  that the
change is  appropriate for the  Multics System, that  it has been
adequately reviewed, that all design problems have been resolved,
and that  the change fits with  the basic Multics goals  for data
security  and  integrity,  system  reliability,  availability and
performance, etc.  For more information, see MAB-048-02 [ref 2].


3.4:  Auditor

After  a  change  has   been  designed,  technically  documented,
approved,  implemented  and  tested,  the  developer  submits the
implemented change to an auditor.   The auditor is a knowledgable
peer who reviews and approves how the change is implemented.  The
auditor  ensures   that  the  change  functions   correctly  with
reasonable  performance, and  adheres to  coding standards.   The
developer and  auditor mutually decide  that the change  is ready
for  installation.  Then the  auditor submits the  audited change
for further  approval and installation.  See MAB-069  [ref 3] for
more information.


3.5:  Security Coordinator

The Security Coordinator is a  person knowledgable in the Multics
TCB and  the security mechanisms implemented by  it.  This person
is involved in the review of proposed changes to the TCB, and may
disallow any  change which would violate  security.  The Security
Coordinator works with the System Integration Project to maintain
the  hierarchy  under  >security,  which  contains  the  suite of
functional tests and associated  documentation.  See MAB-071 [ref
9] for more information.


3.6:  Documentation Personnel

Documentation personnel are involved  in creating and maintaining
Multics customer documentation,  Multics design documentation and
Multics  Software Release   Bulletins (SRBs).   The documentation
manager must  approve changes.  Each Multics  system release must
have  associated  documentation  which  accurately  reflects  the
functionality and design changes made in that release.







                              - 4 -

Configuration Management Policy                           MAB-070


3.7:  Software Development Manager

The development manager must approve a developer's proposal prior
to any formal design work.   Once this approval is granted, MTBs,
design  reviews, MCRs,  etc.   follow.   The manager  must remain
informed   of  the   status  of   each  project   which  is   his
responsibility as  work proceeds from inception  to installation.
Each  developer's manager  is responsible  for final  approval of
MSCRs for submission to the System Integration Project.


3.8:  System Integration Project

This  project is responsible  for maintaining the  Multics system
libraries, and keeping proper audit trails of installations made.
This   project  creates   master  release   tapes  and   Software
Installation Bulletins.


4:  POLICIES FOR PLANNED CHANGES

Changes  made to  pre-release Multics   software as  part of  the
normal development cycle (eg, changes  to be incorporated into an
upcoming Multics Release), must follow the procedures outlined in
the following documents:

o    MAB-066 [ref 1]

o    MAB-048 [ref 2]

o    MAB-069 [ref 3]

o    MAB-056 [ref 4]

o    MAB-067 [ref 7]

o    MAB-068 [ref 8]

o    MAB-071 [ref 9]


PLANNED INSTALLATION POLICY:

All Multics  software changes are  installed by personnel  of the
System Integration Project.  These  personnel are responsible for
maintenance  of  the  System  Libraries  and  keep  records which
provide   required  audit   trails.   Proper   operation  of  the
Configuration  Management   process  is  heavily   influenced  by
installation size.   Therefore, large installations  are strongly
discouraged.  The flow of a software submission follows the path:

o    Developer to


                              - 5 -

MAB-070                           Configuration Management Policy


o    Auditor to

o    Security Coordinator (if TCB) to

o    Documentation to

o    Developer's manager to

o    System Integration Project


DESIGN REVIEW POLICY:

All  Multics software changes  must undergo either  formal design
review(s)  via MTBs  and technical   peer review  board, or  less
formal  review by  the GMCRB.   The developer  is responsible for
correcting any  problems in design and  updating documentation to
reflect that design.

DESIGN APPROVAL POLICY:

The  Multics  Change  Review  Boards  (GMCRB  and  EMCRB) are the
official governing bodies which act  to approve, modify or reject
Multics software change proposals.

CODING STANDARDS POLICY:

All  Multics software  changes  are  subject to  coding standards
published in  MAB-068 [ref.  8].   The developer and  auditor are
responsible for  ensuring that software produced  for the Multics
System  is of  high quality  and respects  these standards.   The
Multics Security Coordinator is responsible for ensuring that TCB
and trusted application code is  produced which abides by Multics
security standards.

TESTING STANDARDS POLICY:

All  Multics  software  changes  must  be  tested  to assure high
quality  and  correct  operation.    For  non-TCB  software,  the
developer  is  responsible  for   testing,  and  the  auditor  is
responsible  for  validation  of   the  test  results.   For  TCB
software,  all or part  of the Functional  Test Suite is  used to
assure  that the software  operates correctly.  The  auditor must
review and validate the test results  to the best of his ability.
The  Multics Security  Coordinator is  ultimately responsible for
validation of Functional Test results.

AUDIT POLICY:

All Multics software changes  must be audited before installation
into  the System  Libraries.  Once  code has  been submitted  for
audit, the auditor removes access for the developer.  The auditor


                              - 6 -

Configuration Management Policy                           MAB-070


is responsible  for reviewing and approving the  changes, and for
ensuring that  the developer corrects all  problems.  An auditing
checklist showing  the results of  the audit must  be attached to
the  Multics  System  Change  Request   (MSCR)  as  part  of  the
submission.   The auditor  submits  the  checklist and  all other
parts of the MSCR to the Documentation Unit manager.


5:  POLICIES FOR EMERGENCY CHANGES

In addition to the documents cited for planned changes, emergency
changes  made  to  pre-release  software  must  follow procedures
described in MAB-057 [ref 5].

EMERGENCY CHANGE POLICY:

When  the  developer  and   responsible  manager  determine  that
software has been installed which:

o    Causes a system to crash;

o    Irreparably compromises data security or integrity, or;

o    Prevents important subsystems from operating;

the developer  must prepare a  fix and submit  an Emergency MSCR.
If  such a problem  is encountered within  two weeks of  a normal
installation,  then a  PBF must  be prepared.   Code suitable for
Emergency MSCR submission may be installed:

o    Without an approved MCR;

o    Without an MSCR form;

o    Without full auditing of the code;

o    Without documentation or project approvals;

Within  five  working  days  of  the  emergency installation, the
developer will resubmit the  fix through the planned installation
process.  At  that point, all policies  in Section 4 of  this MAB
apply.


6:  POLICIES FOR CRITICAL FIX CHANGES

Critical fix  changes made to an existing  Multics system release
must follow procedures described in MAB-063 [ref 6].

CRITICAL FIX POLICY:




                              - 7 -

MAB-070                           Configuration Management Policy


When the developer and responsible  unit manager determine that a
critical problem  exists in released software, they  select a set
of  Multics Releases for  which the problem  will be fixed.   The
developer creates and  tests the fix, and has  it audited.  Fixes
affecting the  TCB must be reviewed by  the Security Coordinator,
as  well,  to  determine  correctness  of  the  fix, and to judge
whether  the problem  being fixed   represents a  breach of  data
security.   After the  Auditor and  Security Coordinator reviews,
the fix is given to the Critical Fix Coordinator for installation
in the  fix library and distribution to  customers.  The Critical
Fix Coordinator is responsible  for restricting information about
data security  problems until customers have had  time to correct
these problems.

The  procedures  which  implement  this  policy  are described in
MAB-063 [ref 6].






































                              - 8 -
