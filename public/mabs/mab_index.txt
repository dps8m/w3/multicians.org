
MULTICS ADMINISTRATIVE BULLETIN CONTENTS               1985-08-27


          Number    Date        Subject


MAB006    1974-01-02  Design Reviews
MAB020    1975-02-07  Documentation Hierarchy
MAB021    1975-04-10  ios_ to iox_ Conversions
MAB023    1975-04-02  Multics Software Changes
MAB024    1975-05-07  Naming Conventions
MAB031    1976-06-06  Publication of Approved MCR's
MAB032-01 1982-11-18  Experimental Library (EXL)
MAB041-01 1978-10-11  Manual Specifications
MAB042-01 1978-05-16  Documentation Production Cycle
MAB044-01 1981-02-13  Goals   for   Processing   Multics  Trouble
                      Reports
MAB045    1979-05-31  Trip Reports
MAB047    1979-09-19  info_segs
MAB048-02 1985-08-12  Rules for the Multics Change Review Board
MAB049-01 1980-06-11  TR Closure and Resolution
MAB051-05 1982-09-02  Phoenix Multics Change Review Board
MAB052-01 1984-01-25  Standards for Software Protection and STI
MAB053    1981-10-05  Installation of info segs
MAB054    1981-07-08  Standardized Multics Error Lists
MAB055    1982-08-03  Multics Standards and Techniques Committee
MAB056-02 1985-08-07  Multics      Configuration      Management:
                      Installing   Planned   Changes   in  System
                      Libraries
MAB057-02 1985-08-07  Multics      Configuration      Management:
                      Installing  Emergency   Changes  in  System
                      Libraries
MAB058    1982-12-22  Multics     Technical    Bulletin     (MTB)
                      Publication
MAB062    1984-04-23  MR11 Freeze Dates
MAB063-01 1985-08-12  Multics Configuration Management:  Critical
                      Fixes for Released Software
MAB064    1984-08-27  Third Party Software Submission Procedures
MAB065-01 1984-10-23  Using    Forum   to    Facilitate   Multics
                      Development Activities
MAB066    1985-08-08  Multics Configuration Management:  Software
                      Development
MAB067    1985-08-05  Procedures  for  Software  Installation and
                      Integration
MAB068    1985-08-13  Multics      Configuration      Management:
                      Programming Standards
MAB069    1985-08-13  Multics      Configuration      Management:
                      Guidelines for Auditing Software
MAB070    1985-08-14  Multics  Configuration Management:   Policy
                      Statement
MAB071    1985-08-14  Multics  Security Coordinator:   Duties and
                      Responsibilities





                               -i-
MULTICS ADMINISTRATIVE BULLETIN SUMMARIES              1985-08-27


MAB006,   1974-01-02,  _D_e_s_i_g_n   _R_e_v_i_e_w_s_,  Clingen:    Sets  forth
      guidelines  for   determining  when  a  design   review  is
      required.

MAB020, 1975-02-07, _D_o_c_u_m_e_n_t_a_t_i_o_n _H_i_e_r_a_r_c_h_y_, Erickson:  Describes
      in written  and pictorial forms  where to find  the Multics
      System Documentation (Manuals).

MAB021, 1975-04-10, _i_o_s__ _t_o  _i_o_x__ _C_o_n_v_e_r_s_i_o_n_s_, Gintell:  Requires
      the use of iox_ for installation but, gives acceptions.

MAB023,  1975-04-02,  _M_u_l_t_i_c_s  _S_o_f_t_w_a_r_e  _C_h_a_n_g_e_s_,  Berglund, May:
      Decribes Multics documents and procedures to be followed in
      processing a Multics software change.

MAB024, 1975-05-07, _N_a_m_i_n_g  _C_o_n_v_e_n_t_i_o_n_s_, May, Clingen:  Expresses
      concern about software naming  conventions and suggests the
      use of an underscore for word separation.

MAB031,  1976-06-06,  _P_u_b_l_i_c_a_t_i_o_n  _o_f  _A_p_p_r_o_v_e_d  _M_C_R_'_s_,  Clingen:
      Multics Change Requests will  be published separately every
      two weeks  and distributed only to  those people requesting
      them.

MAB032-01,  1982-11-18,  _E_x_p_e_r_i_m_e_n_t_a_l  _L_i_b_r_a_r_y  _(_E_X_L_)_,  Benjamin:
      Establishes  guidelines  for  the  use  of the experimental
      library.

MAB041-01, 1978-10-11, _M_a_n_u_a_l _S_p_e_c_i_f_i_c_a_t_i_o_n_s_, Steve Webber:  Step
      by step tutorial for filling out the form.

MAB042-01,  1978-05-16,  _D_o_c_u_m_e_n_t_a_t_i_o_n  _P_r_o_d_u_c_t_i_o_n  _C_y_c_l_e_,  Steve
      Webber:   Describes  the  product  cycle  of  documentation
      manuals  and   the  responsibilities  of   the  individuals
      involved with it.

MAB044-01,  1981-02-13,  _G_o_a_l_s  _f_o_r  _P_r_o_c_e_s_s_i_n_g  _M_u_l_t_i_c_s  _T_r_o_u_b_l_e
      _R_e_p_o_r_t_s_,   GDixon,    Gintell,   vonSeeBurg:    Establishes
      guidelines for processing Trouble Reports.

MAB045, 1979-05-31, _T_r_i_p _R_e_p_o_r_t_s_, Gintell:  Establishes procedure
      to follow when writing a Trip Report.

MAB047,  1979-09-19, _i_n_f_o___s_e_g_s_, GJohnson:   Establishes procedure
      for maintenance  and installation of  information segments.
      Includes the form.

MAB048-02, 1985-08-12, _R_u_l_e_s _f_o_r _t_h_e _M_u_l_t_i_c_s _C_h_a_n_g_e _R_e_v_i_e_w _B_o_a_r_d_,
      RBarstad:   Defines  the  structure  of  the Multics Change
      Review Board (MCRB) and the  procedures that the board, and
      others doing business with the  board, are to follow in the
      course of processing Multics Change Requests (MCRs).


                               -ii-
MULTICS ADMINISTRATIVE BULLETIN SUMMARIES              1985-08-27


MAB049-01,  1980-06-11,  _T_R  _C_l_o_s_u_r_e  _a_n_d  _R_e_s_o_l_u_t_i_o_n_, Martinson,
      GDixon:    Establishes   guidelines   for   Trouble  Report
      processing.

MAB051-05,  1982-09-02,  _P_h_o_e_n_i_x  _M_u_l_t_i_c_s  _C_h_a_n_g_e  _R_e_v_i_e_w  _B_o_a_r_d_,
      Benjamin:   The charter of  the PMCRB.  Details  who, what,
      where, why and how.

MAB052-01, 1984-01-25, _S_t_a_n_d_a_r_d_s _f_o_r _S_o_f_t_w_a_r_e _P_r_o_t_e_c_t_i_o_n _a_n_d _S_T_I_,
      Martinson,  Stansbury,  Holmstedt:   Lists  references  for
      other documents  detailing offical policies with  regard to
      Software Protection and Software Technical Identifiers.

MAB053,   1981-10-05,  _I_n_s_t_a_l_l_a_t_i_o_n   _o_f  _i_n_f_o   _s_e_g_s_,  GJohnson:
      Describes in detail the  procedure for installation of info
      segs which ARE NOT a result of an approved MCR.

MAB054,  1981-07-08, _S_t_a_n_d_a_r_d_i_z_e_d   _M_u_l_t_i_c_s _E_r_r_o_r  _L_i_s_t_s_, Texada:
      This bulletin defines the  requirement for, and the format,
      content, and control of Standard Multics Error Lists.

MAB055, 1982-08-03,  _M_u_l_t_i_c_s _S_t_a_n_d_a_r_d_s _a_n_d  _T_e_c_h_n_i_q_u_e_s _C_o_m_m_i_t_t_e_e_,
      Batts,  Benjamin,  GDixon,  Herbst,  CLJones,  Palter:  The
      charter for the MSaTC.  Includes  who, what, where, why and
      how.

MAB056-02,   1985-08-07,    _M_u_l_t_i_c_s   _C_o_n_f_i_g_u_r_a_t_i_o_n   _M_a_n_a_g_e_m_e_n_t_:
      _I_n_s_t_a_l_l_i_n_g _P_l_a_n_n_e_d _C_h_a_n_g_e_s  _i_n _S_y_s_t_e_m _L_i_b_r_a_r_i_e_s_, Martinson,
      GJohnson:    Defines  procedures  for   submitting  planned
      software changes  for installation into the  Multics System
      Libraries.

MAB057-02,   1985-08-07,    _M_u_l_t_i_c_s   _C_o_n_f_i_g_u_r_a_t_i_o_n   _M_a_n_a_g_e_m_e_n_t_:
      _I_n_s_t_a_l_l_i_n_g _E_m_e_r_g_e_n_c_y _C_h_a_n_g_e_s _i_n _S_y_s_t_e_m _L_i_b_r_a_r_i_e_s_, GJohnson:
      Defines  procedures for  submitting software  for emergency
      installation into the Multics System Libraries.

MAB058, 1982-12-22, _M_u_l_t_i_c_s _T_e_c_h_n_i_c_a_l _B_u_l_l_e_t_i_n _(_M_T_B_) _P_u_b_l_i_c_a_t_i_o_n_,
      Gintell, Coren:  Newest  procedures for preparing approving
      and distributing Multics Technical Bulletins (MTB's).

MAB062,  1984-04-23, _M_R_1_1  _F_r_e_e_z_e _D_a_t_e_s_,  Martinson:  Agreed upon
      dates  for  the  finalization  of  Multics  software  to be
      included in Multics Release 11.

MAB063-01,   1985-08-12,    _M_u_l_t_i_c_s   _C_o_n_f_i_g_u_r_a_t_i_o_n   _M_a_n_a_g_e_m_e_n_t_:
      _C_r_i_t_i_c_a_l  _F_i_x_e_s _f_o_r  _R_e_l_e_a_s_e_d _S_o_f_t_w_a_r_e_,  GDixon:  Describes
      policies  and  procedures  for  preparing  and distributing
      critical fixes  to sites.  Critical fixes  are grouped into
      two categories, based upon their impact on system security.
      The categories  are nonsecurity problems and  data security
      problems.


                              -iii-
MULTICS ADMINISTRATIVE BULLETIN SUMMARIES              1985-08-27


MAB064, 1984-08-27,  _T_h_i_r_d _P_a_r_t_y _S_o_f_t_w_a_r_e  _S_u_b_m_i_s_s_i_o_n _P_r_o_c_e_d_u_r_e_s_,
      Martinson:  defines  policy and procedure  for installation
      of  third party  software.  Guidelines  for acquiring Third
      Party Packages  are not part  of this MAB.   This procedure
      assumes   acquisition   of   software   by   Marketing  and
      concurrence by  MDC has already occured.  This  MAB is also
      intended  for release to  vendors to provide  definition of
      MDC requirements to which vendors  must conform to prior to
      installation in standard system libraries.

MAB065-01,   1984-10-23,  _U_s_i_n_g   _F_o_r_u_m  _t_o   _F_a_c_i_l_i_t_a_t_e  _M_u_l_t_i_c_s
      _D_e_v_e_l_o_p_m_e_n_t _A_c_t_i_v_i_t_i_e_s_, Clingen:   Describes guidelines for
      using forum to discuss Multics software development issues.

MAB066,  1985-08-08, _M_u_l_t_i_c_s _C_o_n_f_i_g_u_r_a_t_i_o_n  _M_a_n_a_g_e_m_e_n_t_:  _S_o_f_t_w_a_r_e
      _D_e_v_e_l_o_p_m_e_n_t_,    Margulies,     RBarstad:     details    the
      configuration management  procedures to be  followed during
      the development of Multics software.

MAB067,  1985-08-05,  _P_r_o_c_e_d_u_r_e_s  _f_o_r  _S_o_f_t_w_a_r_e  _I_n_s_t_a_l_l_a_t_i_o_n _a_n_d
      _I_n_t_e_g_r_a_t_i_o_n_, Sibert:   Describes the process  of installing
      software   changes  into   the  Multics   system  libraries
      maintained  on  Honeywell's   System-M  Multics  system  in
      Phoenix, Arizona.

MAB068,    1985-08-13,    _M_u_l_t_i_c_s    _C_o_n_f_i_g_u_r_a_t_i_o_n    _M_a_n_a_g_e_m_e_n_t_:
      _P_r_o_g_r_a_m_m_i_n_g  _S_t_a_n_d_a_r_d_s_,  RBarstad:   A  description  of the
      standards, conventions, and guidelines used in the software
      and documentation of the Multics operating system.

MAB069, 1985-08-13, _M_u_l_t_i_c_s _C_o_n_f_i_g_u_r_a_t_i_o_n _M_a_n_a_g_e_m_e_n_t_:  _G_u_i_d_e_l_i_n_e_s
      _f_o_r _A_u_d_i_t_i_n_g _S_o_f_t_w_a_r_e_, GDixon:  Details policy for auditing
      of Multics software changes.

MAB070,  1985-08-14,  _M_u_l_t_i_c_s  _C_o_n_f_i_g_u_r_a_t_i_o_n  _M_a_n_a_g_e_m_e_n_t_:  _P_o_l_i_c_y
      _S_t_a_t_e_m_e_n_t_,  Clingen,   Stansbury:   Establishes  management
      policy regarding Multics Configuration Management.

MAB071,  1985-08-14, _M_u_l_t_i_c_s   _S_e_c_u_r_i_t_y _C_o_o_r_d_i_n_a_t_o_r_:   _D_u_t_i_e_s _a_n_d
      _R_e_s_p_o_n_s_i_b_i_l_i_t_i_e_s_,  Stansbury:  This bulletin  describes the
      position  responsibilities   and  duties  of   the  Multics
      Security Coordinator.












                               -iv-
MULTICS ADMINISTRATIVE BULLETIN HISTORICAL INDEX       1985-08-27


Number    Status


MAB001    Expired
MAB002    Replaced by MAB-048
MAB003    Replaced by MAB-034
MAB004-01 Replaced by MAB-056
MAB005    Replaced by MAB-034
MAB006    Hard Copy Only
MAB007    Expired
MAB008    Expired
MAB010    Expired
MAB011    Expired
MAB012    Replaced by MAB-056
MAB013    Replaced by MAB-056
MAB014    Replaced by MAB-056
MAB015    Replaced by MAB-056
MAB016    Expired
MAB017    Replaced by MAB-048
MAB018    Replaced by MAB-048
MAB019    Replaced by MAB-048
MAB020    Hard Copy Only
MAB021    Hard Copy Only
MAB022    Expired
MAB023    Hard Copy Only
MAB024    Hard Copy Only
MAB025    Expired
MAB026    Replaced by MAB-048
MAB027    Expired
MAB028    Replaced by MAB-061
MAB029    Expired
MAB030    Replaced by MAB-034
MAB031    Hard Copy Only
MAB032-01 Online
MAB033    Unpublished
MAB034    Replaced by MAB-056
MAB035    Expired
MAB036    Expired
MAB037    Expired
MAB038    Expired
MAB039    Unpublished
MAB040    Unpublished
MAB041-01 Hard Copy Only
MAB042-01 Hard Copy Only
MAB043    Expired
MAB044-01 Online
MAB045    Hard Copy Only
MAB046    Expired
MAB047    Hard Copy Only
MAB048-02 Online
MAB049-01 Online
MAB050    Unpublished


                               -v-
MULTICS ADMINISTRATIVE BULLETIN HISTORICAL INDEX       1985-08-27


Number    Status


MAB051-05 Online
MAB052-01 Online
MAB053    Hard Copy Only
MAB054    Hard Copy Only
MAB055    Online
MAB056-02 Online
MAB057-02 Online
MAB058    Online
MAB062    Online
MAB063-01 Online
MAB064    Online
MAB065-01 Online
MAB066    Online
MAB067    Online
MAB068    Online
MAB069    Online
MAB070    Online
MAB071    Online
MAB072    Unpublished
































                               -vi-