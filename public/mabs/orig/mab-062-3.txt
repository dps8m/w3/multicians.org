MULTICS ADMINISTRATIVE BULLETIN                         MAB062-03


To:       MAB Distribution

From:     F. W. Martinson

Date:     November 9, 1987

Subject: MR12.2 Freeze Dates


Abstract:

Dates for the Finalization of  Multics Software to be included in
Multics Release 12.2.

Revisions:

      (Rev3)  11/87,   F.   W.   Martinson   Establishes  MR12.2
      software Finalization dates.


The following  are the freeze  and release dates  for MR12.2.  If
you  cannot   meet  these  dates  for  SCP   or  other  scheduled
development  implementation, you  should immediately  inform your
manager.

1.  Final submission date - Oct.  7 (FW840)

2.  Freeze date (installed) - Oct.  14 (FW841)

3.  General Release - Dec.  2 (FW848)


















_________________________________________________________________

Multics  Project  internal  working  documentation.   Not  to  be
reproduced or distributed outside the Multics Project.
