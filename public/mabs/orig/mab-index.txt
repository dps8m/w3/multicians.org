
MULTICS ADMINISTRATIVE BULLETIN                        MAB-INDEX


To:  Distribution

From:  Richard Holmstedt

Date:  07/19/84

Subject:  Multics Administrative Bulletin (MAB) Index.


_I_N_T_R_O_D_U_C_T_I_O_N


 This  bulletin  is  intended  to  provide  a  current  status of
published MABs.  See MAB-061 for detail.


MAB-001 -EXPIRED-
MAB-002 Replaced by MAB-048
MAB-003 Replaced by MAB-034
MAB-004 Replaced by MAB-056
MAB-005 Replaced by MAB-034
MAB-006 Design Reviews.
MAB-007 -EXPIRED-
MAB-008 -EXPIRED-
MAB-009 -EXPIRED-
MAB-010 -EXPIRED-
MAB-011 -EXPIRED-
MAB-012 Replaced by MAB-056
MAB-013 Replaced by MAB-056
MAB-014 Replaced by MAB-056
MAB-015 New System Change Request Forms
MAB-016 -EXPIRED-
MAB-017 Replaced by MAB-048
MAB-018 Replaced by MAB-048
MAB-019 Replaced by MAB-048
MAB-020 Documentation Hierarchy.
MAB-021 Conversion of programs to use ios_ interfaces instead of ioa_.
MAB-022 -EXPIRED-
MAB-023 A Review of Procedures for Multics Software Changes.
MAB-024 Multics Software Naming Conventions.
MAB-025 -EXPIRED-
MAB-026 Replaced by MAB-048
MAB-027 -EXPIRED-
MAB-028 Replaced by MAB-061
MAB-029 -EXPIRED-
MAB-030 Replaced by MAB-034
MAB-031 Publication of Approved MCRs.

________________________________________

Multics  Project  Internal  working  Documentation.   Not  to  be
reproduced or distributed outside of the Multics project.


                              Page 1
MULTICS ADMINISTRATIVE BULLETIN                        MAB-INDEX


MAB-032 The Experimantal Library. (Rev. 1)
MAB-033 -Unpublished- (See MAB-034)
MAB-034 Changes to the Software Submission Procedures.
MAB-035 -EXPIRED-
MAB-036 -EXPIRED-
MAB-037 -EXPIRED-
MAB-038 -EXPIRED-
MAB-039 -Unpublished-
MAB-040 -Unpublished-
MAB-041 Filling Out Manual Specification. (Rev. 1)
MAB-042 Documentation Production Cycle (Rev. 1)
MAB-043 -EXPIRED-
MAB-044 Goals for Processing TRs (Rev. 1)
MAB-045 Trip Reports
MAB-046 -EXPIRED-
MAB-047 Procedures for maintenance and installation of info_segs.
MAB-048 Rules for the MCR Boards. (Rev. 1)
MAB-049 TR Closure and Resolution. (Rev. 1)
MAB-050 -Unpublished-
MAB-051 Rules for Phoenix MCR Board and Phoenix MCR Packet. (Rev. 5)
MAB-052 Standards for Software Protection and STI.
MAB-053 Installation of Info Segments.
MAB-054 Standardized Multics Error List.
MAB-055 Multics Standards and Techniques Committee.
MAB-056 Procedures for Normal Installations.
MAB-057 Procedures for Emergency Installations.
MAB-058 Multics Technical Bulletin (MTB) Publication.
MAB-059 Generation of Micro-fiche for Multics Software.
MAB-060 Multics Development Release Plans.
MAB-061 Multics Administrative Bulletin (MAB) Publication.
MAB-062 Freeze dates
MAB-056 Procedures for Normal Installations
MAB-057 Procedures for Emergency Installations
MAB-062 Revised MR11 Freeze Dates




















                              Page 2