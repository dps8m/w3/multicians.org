
MULTICS ADMINISTRATIVE BULLETIN                           MAB-064


To:       MAB Distribution

From:     F. W. Martinson

Date:     August 27, 1984

Subject:  Third Party Software Submission Procedures




Purpose

To define  policy and procedure  for installation of  third party
software.  Guidelines for acquiring  Third Party Packages are not
part of this MAB.  This procedure assumes acquisition of software
by Marketing  and concurrence by  MDC has already  occured.  This
MAB is also intended for release to vendors to provide definition
of  MDC requirements  to which vendors  must conform  to prior to
installation in standard system libraries.

Types of Third Party Software

There are two  types of third party software.   The first will be
called  "Integrated  Software",  and  the  second "Self-Contained
Software".

Integrated Software  is software developed under  contract to MDC
by non-MDC  organizations (e.g.  fortran,  cobol) OR applications
software that requires software changes to Multics to support the
package (e.g.  forum)  OR which executes in any  inner ring (e.g.
forum).   Packages  in this  category require  a large  degree of
conformance to MDC programming and installation standards.

Self-Contained  Software  is  software that  requires  no special
software  support outside  the package itself  and which executes
none of  its own software  from an inner ring.   Packages in this
category require little or no  conformance to MDC programming and
installation standards.  The primary requirement for installation
of a package  in this category will be  agreement between MDC and
Multics Marketing that this package should be installed.

Normally  selection  of one  of  the above  types for  a specific
package is straight forward and determined by the Manager Multics
Software Support  or designate.  Should a  difference arise as to
which type a specific package belongs, resolution will be between
the  Manager Multics  Software Support  and the  responsible Unit
Manager.   Should differences  remain, resolution  will be sought
________________________________________

Multics  Project  internal  working  documentation.   Not  to  be
reproduced or distributed outside the Multics Project.


                              - 1 -

MAB-064                                      Third Party Software


through  the  Manager Multics  Software Development,  Phoenix and
Manager Multics  Software Development, Cambridge.   If necessary,
final decision will be made  by the Director, Multics Development
Center.

***** Integrated Software - Examples are Fortran and Forum *****

1.   Must  be  installed  in  >system_library_unbundled  and it's
equivalent   source    and   object   directories    within   the
>library_dir_dir  hierarchy.   Include  files  are  installed  in
>library_dir_dir>include.    Info   segments  are   installed  in
>documentation>info_segments   or   >documentation>subsystem   as
appropriate.   For  packaging  purposes  it may  be  necessary to
install executable  pieces of a  product in a  library other than
>system_library_unbundled.  All  pieces of a  third party product
thus installed  will be considered  part of the  standard Multics
product,  bundled  and distributed  to  all users.   Vendors will
retain  ownership  rights,  but   for  distribution  and  revenue
purposes  this  software  will  not  be  considered  part  of the
product.   The module  >system_library_standard>bound_math_ which
is really part of fortran is an example.

2.   Must  conform to  Multics  coding standards.   Currently the
Coding Standards Documentation is  under revision.  When revision
is  complete this  MAB will be  updated to  refer specifically to
that document.

3.  Must undergo  MCR Board scrutiny.  This software  can only be
installed by an approved MCR or by specific management direction.

4.   Must conform  to Documentation  Standards in  the form  of a
standard  Honeywell  Manual.  Usually  there  will be  no special
instructions to install this  software.  Special instructions, if
required, should appear in the Users' Guide for the package.  See
Appendix  A  of the  Forum  Users' Guide  (CY74) for  an example.
Alternatively special instructions can appear in the Installation
Instructions  SRB for  a General Release.   Instruction SRBs will
not be produced for Integrated Software.

5.   Must  conform  to  Packaging  Standards.   Software  must be
installed  in  accordance with  existing  installation standards.
Basically this means MDC must  receive all source and appropriate
installation  forms  (MSCRs)  properly  filled  out.   Source  is
compiled by  MDC.  Object archives  are built by  MDC, which then
are  bound  to  produce  executable  software.   Source archives,
object  archives  and  executable  software  is  then  installed.
Current packaging standards are:

   A.   The  package  must  be contained  in  one  or  more bound
   segments.   The names  must generically  identify the product.
   Each  bound  segment  will  include an  equivalent  source and
   object archive(s).   An exception may  be made in  the case of


                              - 2 -

Third Party Software                                      MAB-064


   source   modules,  where   source  modules   are  specifically
   excluded.  Free  standing segments should be  avoided but will
   be permitted  on an exception basis,  upon mutual agreement by
   both parties.   Mutual agreement normally will  be in the form
   of  an  approved  Multics  Change  Request  (MCR).   Packaging
   examples of names for a hypothetical "frobis" product are:

     bound_frobis_ (Executable bound segment)
     bound_frobis_.archive (Object archive)
     bound_frobis_.s.archive (Source archive)

   B.  Command and subroutine names, added to the primary product
   name,  must  be preceded  by  the generic  name  that uniquely
   identifies the product, followed by an underscore, followed by
   the  functional command  name.  By  convention, all subroutine
   names  are  followed by  a  trailing underscore.   Examples of
   command and subroutine names are:

     frobis_print
     frobis_list
     frobis_delete_

   C.  Segments  within each archive  will have as a  part of the
   beginning  of  each name,  the  generic name  of  the product.
   Examples of segment names are:

     frobis_print.pl1
     frobis_list.pl1
     frobis_delete_.pl1

   D.  Include files  unique to each product will  have as a part
   of  the  beginning  of  each name,  the  generic  name  of the
   product.  Examples of include file names are:

     frobis_data_seg.incl.pl1
     frobis_ptr_dcls.incl.pl1
     frobis_error_names.incl.pl1

6.   Software Support  (problem reporting) will  be performed via
the  TR  System.  The  TR System  will be  used to  route trouble
reports  to  the proper  developer.   The developer  will  be the
vendor of  the package, unless a  separate contractural agreement
has  been  executed.  TR  investigation, response  and resolution
will be performed within guidelines outlined in MAB-044.

7.    Software  is   distributed  solely  by   the  CSD  Software
Distribution  Library and  only when  a request  for distribution
appears  on   the  CSD  Ship  Schedule.    Procedures  exist  for
exceptions  to  this  rule  in the  case  of  corrections  to fix
critical software problems, and release  of software to Beta Test
Sites  or  Controlled  Release  Sites.  The  latter  two examples
require separate agreements to be signed between the customer and


                              - 3 -

MAB-064                                      Third Party Software


Honeywell.  Third party agreements  should include verbiage which
permits these alternate methods of distribution.

8.   Must  conform  to  Honeywell  IS-14  and  Software Technical
Identifier  (STI)  standards.   All  third  party  software  must
contain protection notices and STIs as specified in MAB-052.  The
MDC Software Integration Project  has responsibility for ensuring
proper  placement  of protection  notices  and STIs  in software.
Therefore,  there is  little a  third party  vendor need  to know
about  them other  than that  they must exist.   It is  up to the
third party vendor to supply  the text for the desired protection
notice.  The protection notice must be in Multics standard format
to permit verification and checking  by existing tools.  The name
to be  entered on the  protection notice identifies  ownership of
the  product.   Three  examples  of  standard  format  protection
notices are:

Copyright, (C) Honeywell Information Systems Inc., 1984

Copyright, (C) Honeywell Limited, 1983

Copyright  (c)  1980 by  Centre  Interuniversitaire de  Calcul de
Grenoble  and Institut  National de Recherche  en Informatique et
Automatique

9.  Qualification  of third party software  is the responsibility
of  the  vendor  (owner)  of   the  software  unless,  by  mutual
agreement,  qualification  is  performed   by  MDC.   It  is  the
responsibility  of  the  vendor  to ensure  the  software package
continues  to  run from  release to  release and  is, as  much as
possible, release independent.


***** Self-Contained Software - Examples are SCFF and ISTAT *****

1.   Must  be installed  in the  >system_library_3rd_party (short
name sl3p) library hierarchy.  No  software will be installed nor
will  links be  permitted at the  >sl3p level.   Each third party
package will be wholly contained  in directories below sl3p.  For
example, in  the case of  a "frobis" product  the structure would
be:

  >sl3p>frobis>executable (e)
               include (incl)
               info_segments (info)
               object (o)
               source (s)

Empty  directories  (e.g.   no  source  to  be  shipped)  will be
deleted.  With  the entire package wholly  contained and isolated
in  its  own  structure,   problems  with  naming  conflicts  are
minimized.  These directories will not be in the default standard


                              - 4 -

Third Party Software                                      MAB-064


system search rules.  To use any  of these packages the user must
execute a command such as:

  initiate ([segs >sl3p>frobis>e>* -absp]) -a -fc

to ensure  the correct software is  initiated and name conflicts,
if they  exist, are properly  resolved.  Such a  command could be
part  of  a  user  start_up.ec.    By  definition  no  pieces  of
Self-Contained  Software will  be installed in  any library other
than >sl3p.

2.  May not conform to  Multics coding standards.  Conformance to
standards is recommended but not  required.  If at all possible a
coding standards  manual should be made  available to third party
software vendors.

3.   Software  will not  undergo  MCR Board  scrutiny  or review.
Installation  authorization requires  only a  signed contract AND
approval  of Director,  Multics Development  Center and Director,
Multics  Program  Office or  their  designees.  To  ensure proper
recording and execution of  each installation request, the vendor
of third party  software will be required to  fill out and submit
standard  software  installation forms  (Multics  Software Change
Request).

4.   May   not  conform  to   Documentation  Standards.   Special
instructions  for  the   setup  or  use  of  a   package  is  the
responsibility of the vendor.  Special installation instructions,
must  appear either  as an online  info segment  installed in the
info directory  of the package  itself, or as an  appendix in the
Users' Guide for the package.  See Appendix A of the Forum Users'
Guide (CY74)  for a documentation  example.  Special installation
instructions may not appear  in the Installation Instructions SRB
for a General Release.  Installation Instruction SRBs will not be
produced for Self-Contained Software.


5.  Should conform  to Packaging Standards as outlined  in item 5
for   Integrated  Software.    Conformance  to   these  standards
guarantees  uniqueness  of  module  names  and  therefore  avoids
problems with  conflict of reference names.   Conformance, to the
packaging  standard,  is  not  required  but  is  recommended for
purposes  of efficiency,  ease of  use, ease  of installation and
ease of  maintenance.  Software running  in inner rings  or gates
will not be acceptable under any circumstances for Self-Contained
Software  products.  Software  submitted for  installation in the
>sl3p hierarchy  will be installed  as is, with  the exception of
adding   protection  notices   or  STIs.   Source   will  not  be
recompiled.   All  software  submitted for  installation  will be
considered part  of the third party  package.  Therefore, vendors
not  desiring  to  ship  source to  customers  should  submit for
installation only source that they wish to be distributed.


                              - 5 -

MAB-064                                      Third Party Software


6.   Software Support  (problem reporting) will  be performed via
the  TR  System.  The  TR System  will be  used to  route trouble
reports  to  the proper  developer.   The developer  will  be the
vendor of  the package, unless a  separate contractural agreement
has  been  executed.  TR  investigation, response  and resolution
will be performed within guidelines outlined in MAB-044.

7.    Software  is   distributed  solely  by   the  CSD  Software
Distribution  Library and  only when  a request  for distribution
appears  on   the  CSD  Ship  Schedule.    Procedures  exist  for
exceptions  to  this  rule  in the  case  of  corrections  to fix
critical software problems, and release  of software to Beta Test
Sites  or  Controlled  Release  Sites.  The  latter  two examples
require separate agreements to be signed between the customer and
Honeywell.  Third party agreements  should include verbiage which
permits these alternate methods of distribution.

8.   Must  conform  to  Honeywell  IS-14  and  Software Technical
Identifier  (STI)  standards.   All  third  party  software  must
contain protection notices and STIs as specified in MAB-052.  The
MDC Software Integration Project  has responsibility for ensuring
proper  placement  of protection  notices  and STIs  in software.
Therefore,  there is  little a  third party  vendor need  to know
about  them other  than that  they must exist.   It is  up to the
third party vendor to supply  the text for the desired protection
notice.  The protection notice must be in Multics standard format
to permit verification and checking  by existing tools.  The name
to be  entered on the  protection notice identifies  ownership of
the  product.   Three  examples  of  standard  format  protection
notices are:

Copyright, (C) Honeywell Information Systems Inc., 1984

Copyright, (C) Honeywell Limited, 1983

Copyright  (c)  1980 by  Centre  Interuniversitaire de  Calcul de
Grenoble  and Institut  National de Recherche  en Informatique et
Automatique

9.  Qualification  of third party software  is the responsibility
of the vendor (owner) of  the software.  It is the responsibility
of  the vendor  to ensure the  software package  continues to run
from  release to  release and  is, as  much as  possible, release
independent.  Limited resources will be provided, on System M, to
provide a vehicle for software qualification by the vendor.









                              - 6 -
